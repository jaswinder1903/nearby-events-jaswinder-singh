# Project Title

Near By Events using third party API's.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Just clone and add your Google Map APi and you are ready to go.

## Built With

* [GoogleCloud](https://cloud.google.com/) - For autocomplete suggestion in location.
* [ticketmaster](hhttps://developer.ticketmaster.com/) - API with event details.

## Authors

* **JaswinderSingh** 
* **jaswinder.singh@classicinformatics.com** 

