$(document).ready(function() {
    const apiKey = 'AGUhngTkGDjGCeGWfeWJxKoFOyPOeywV';
    /* Get Event Name */
    getEventName();

    /* On submit form to fetch events */
    $("#SearchForm").submit(function(event) {
        $('#loader').show();
        event.preventDefault();
        var formData = $("form").serializeArray();
        var data = {};
        $(formData).each(function(index, obj) {
            data[obj.name] = obj.value;
        });

        var geoPoint = data.geohash;
        var radius = data.radius;
        var unit = 'miles';
        var classificationName = data.eventList;

        $.ajax({
            type: "GET",
            url: 'https://app.ticketmaster.com/discovery/v2/events.json?apikey=' + apiKey + '&geoPoint=' + geoPoint + '&radius=' + radius + '&unit=' + unit + '&classificationName=' + classificationName,
            async: true,
            dataType: "json",
            success: function(response) {
                $("#eventCard").html('');
                if (response._embedded) {
                    var attractions = response._embedded.attractions
                    var attractions = response._embedded.events
                    var links = response._links
                    var page = response.page
                    $.each(attractions, function(key, value) {
                        makeEventhtml(key, value)
                    });
                } else {
                    $("#eventCard").append('<span>No Event Found.</span>');
                }
                $('#loader').hide();
            },
            error: function(xhr, status, err) {
                $("#eventCard").append('<span>Something Went Wrong!</span>');
                ('#loader').hide();
            }
        });
    });

    /* API hit to get event name */
    function getEventName() {
        $.ajax({
            type: "GET",
            url: "https://app.ticketmaster.com//discovery/v2/classifications.json?apikey=" + apiKey,
            async: true,
            dataType: "json",
            success: function(response) {

                var name;
                if (response._embedded && response._embedded.classifications) {
                    var classifications = response._embedded.classifications;
                    $.each(classifications, function(key, value) {
                        if (value.type) {
                            name = value.type.name;
                        } else {
                            name = value.segment.name;
                        }
                        $('#eventList').append($("<option></option>")
                            .attr("value", name)
                            .text(name));
                    });
                }
                $('#loader').hide();
            },
            error: function(xhr, status, err) {
                $('#loader').hide();
            }
        });
    }

    /* Make and append HTML of the events */
    function makeEventhtml(key, value) {
        if (value.dates && value.dates.start && value.dates.start.localDate) {
            var startDate = value.dates.start.localDate;
        }
        if (value.images) {
            image = value.images[0].url;
        }
        $("#eventCard").append('<div class="col-md-4 my-3"><div class="card">  <img class="card-img-top" src=' + image + ' alt="Card image cap"><div class="card-body"><h4 class="card-title">' + value.name + '</h4><p class="text-muted">' + startDate + '</p><button type="button" id=' + value.id + ' class="btn btn-outline-primary btn-sm eventdetail">View Details</button> </div> </div></div>');
        return;
    }

    /* Get Geo hash from lat log */
    function encode(lat, lon, precision) {
        const base32 = '0123456789bcdefghjkmnpqrstuvwxyz';
        // infer precision?
        if (typeof precision == 'undefined') {
            // refine geohash until it matches precision of supplied lat/lon
            for (let p = 1; p <= 12; p++) {
                const hash = Geohash.encode(lat, lon, p);
                const posn = Geohash.decode(hash);
                if (posn.lat == lat && posn.lon == lon) return hash;
            }
            precision = 12; // set to maximum
        }

        lat = Number(lat);
        lon = Number(lon);
        precision = Number(precision);

        if (isNaN(lat) || isNaN(lon) || isNaN(precision)) throw new Error('Invalid geohash');

        let idx = 0; // index into base32 map
        let bit = 0; // each char holds 5 bits
        let evenBit = true;
        let geohash = '';

        let latMin = -90,
            latMax = 90;
        let lonMin = -180,
            lonMax = 180;

        while (geohash.length < precision) {
            if (evenBit) {
                // bisect E-W longitude
                const lonMid = (lonMin + lonMax) / 2;
                if (lon >= lonMid) {
                    idx = idx * 2 + 1;
                    lonMin = lonMid;
                } else {
                    idx = idx * 2;
                    lonMax = lonMid;
                }
            } else {
                // bisect N-S latitude
                const latMid = (latMin + latMax) / 2;
                if (lat >= latMid) {
                    idx = idx * 2 + 1;
                    latMin = latMid;
                } else {
                    idx = idx * 2;
                    latMax = latMid;
                }
            }
            evenBit = !evenBit;

            if (++bit == 5) {
                // 5 bits gives us a character: append it and start over
                geohash += base32.charAt(idx);
                bit = 0;
                idx = 0;
            }
        }

        return geohash;
    }

    /* Get Location using autocomplete */
    const precision = 9;
    var input = document.getElementById('city');
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.setComponentRestrictions({
        'country': ['us']
    });
    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        var lat = place.geometry.location.lat(),
            lng = place.geometry.location.lng();
        $('#latitude').val(lat);
        $('#longitude').val(lng);
        const geohash = encode(lat, lng, precision);
        $('#geohash').val(geohash);
    });

    /* Get event details */
    $('body').on('click', '.eventdetail', function() {
        var eventId = $(this).attr('id');
        $.ajax({
            type: "GET",
            url: "https://app.ticketmaster.com/discovery/v2/events/" + eventId + ".json?apikey=" + apiKey,
            async: true,
            dataType: "json",
            success: function(response) {
                var eventname = response.name;
                var pleaseNote = response.pleaseNote;
                $('#desceventname').html(eventname);
                $('#descpleaseNote').html(pleaseNote ? '<b>Please Note:</b> ' + pleaseNote : '<b>Please Note:</b> N/A');
                $('#eventModal1').modal('show');
                var priceRanges = response.priceRanges;
                var seatmap = response.seatmap;
                var ticketLimit = response.ticketLimit;
                if (ticketLimit) {
                    if (ticketLimit.info) {
                        $('#descticketlimit').html('<b>Ticket Limit: </b>' + ticketLimit.info);
                    }
                }
                if (seatmap) {
                    if (seatmap.staticUrl) {
                        $('#descseatmap').html('<b>Seat Map: </b><a target="_blank" href="' + seatmap.staticUrl + '"</a>' + seatmap.staticUrl);
                    }
                }
                if (priceRanges) {
                    $.each(priceRanges, function(key, value) {
                        var innerHtml = '';
                        innerHtml += '<b>Currency: </b>' + value.currency;
                        innerHtml += ', <b>Max: </b>' + value.max;
                        innerHtml += ', <b>Min: </b>' + value.min;
                        innerHtml += ', <b>Type: </b>' + value.type;
                        $('#descpricerange').html(innerHtml);
                    });
                }
            },
            error: function(xhr, status, err) {
                $("#eventCard").append('<span>Something Went Wrong!</span>');
            }
        });
    });
});